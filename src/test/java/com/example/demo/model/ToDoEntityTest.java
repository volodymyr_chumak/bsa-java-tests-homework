package com.example.demo.model;

import org.junit.jupiter.api.Test;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ToDoEntityTest {
    @Test
    void whenToString_thenReturn() {
        var toDoEntity = new ToDoEntity(0L, "test", ZonedDateTime.now(ZoneOffset.UTC));

        //call
        var result = toDoEntity.toString();

        //validate
        assertEquals(result, String.format(
                "ToDoEntity[id=%d, text='%s', completedAt='%s']",
                toDoEntity.getId(), toDoEntity.getText(), toDoEntity.getCompletedAt()));
    }
}
