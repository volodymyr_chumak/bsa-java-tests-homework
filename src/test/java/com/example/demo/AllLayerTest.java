package com.example.demo;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.example.demo.helpers.JsonHelper.asJsonString;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class AllLayerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ToDoRepository toDoRepository;

    @Test
    void whenGetAll_thenReturnValidResponse() throws Exception {
        addDataToDb();

        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$[0].text").exists())
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].completedAt").exists());
    }

    private void addDataToDb() {
        List<ToDoEntity> todos = new ArrayList<>();
        todos.add(new ToDoEntity(1L, "text1 completed", ZonedDateTime.now()));
        todos.add(new ToDoEntity(2L, "text2"));
        todos.add(new ToDoEntity(3L, "text3 completed", ZonedDateTime.now()));
        todos.add(new ToDoEntity(4L, "text4"));

        toDoRepository.saveAll(todos);
    }

    @Test
    void whenSaveNoId_thenReturnValidResponse() throws Exception {
        var generatedId = 5;
        String text = "test todo";
        var saveRequest = new ToDoSaveRequest();
        saveRequest.text = text;

        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(saveRequest))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(generatedId))
                .andExpect(jsonPath("$.text").value(saveRequest.text))
                .andExpect(jsonPath("$.completedAt").doesNotExist());

        toDoRepository.deleteById(5L);
    }

    @Test
    void whenSaveWithId_thenReturnValidResponse() throws Exception {
        addDataToDb();
        var toDo = new ToDoEntity(2L, "text");
        var saveRequest = ToDoEntityToResponseMapper.map(toDo);


        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(saveRequest))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(saveRequest.id))
                .andExpect(jsonPath("$.text").value(saveRequest.text))
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }

    @Test
    void whenComplete_thenReturnValidResponseWithCompletedAt() throws Exception {
        addDataToDb();

        this.mockMvc.perform(MockMvcRequestBuilders
                .put("/todos/{id}/complete", 2)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(2))
                .andExpect(jsonPath("$.text").value("text2"))
                .andExpect(jsonPath("$.completedAt").exists());
    }

    @Test
    void whenGetOne_thenReturnValidResponse() throws Exception {
        addDataToDb();

        this.mockMvc
                .perform(get("/todos/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.text").value("text1 completed"))
                .andExpect(jsonPath("$.completedAt").exists());
    }

}
