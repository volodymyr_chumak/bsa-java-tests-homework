package com.example.demo.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import static org.mockito.Mockito.*;
//import static org.mockito.AdditionalAnswers.*;
import org.mockito.ArgumentMatchers;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;

class ToDoServiceTest {

	private ToDoRepository toDoRepository;

	private ToDoService toDoService;

	//executes before each test defined below
	@BeforeEach
	void setUp() {
		this.toDoRepository = mock(ToDoRepository.class);
		toDoService = new ToDoService(toDoRepository);
	}

	@Test
	void whenGetAll_thenReturnEmpty() {
		//mock
		var testToDos = new ArrayList<ToDoEntity>();
		when(toDoRepository.findAll()).thenReturn(testToDos);

		//call
		var todos = toDoService.getAll();

		//validate
		assertTrue(todos.isEmpty());
	}

	@Test
	void whenUpsertWithId_thenThrowNotFoundException() {
		var notExistentId = 0L;
		var toDoSaveRequest = new ToDoSaveRequest();
		toDoSaveRequest.id = notExistentId;
		toDoSaveRequest.text = "todo for test";

		//validate
		assertThrows(ToDoNotFoundException.class, () -> toDoService.upsert(toDoSaveRequest));
	}

	@Test
	void whenCompleteToDoNotFound_thenThrowNotFoundException() {
		assertThrows(ToDoNotFoundException.class, () -> toDoService.completeToDo(0L));
	}

}
