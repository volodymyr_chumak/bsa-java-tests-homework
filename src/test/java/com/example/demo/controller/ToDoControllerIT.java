package com.example.demo.controller;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.model.ToDoEntity;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;

import static com.example.demo.helpers.JsonHelper.asJsonString;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
class ToDoControllerIT {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ToDoService toDoService;

	@Test
	void whenSaveNoId_thenReturnValidResponse() throws Exception {
		String text = "test todo";
		var saveRequest = new ToDoSaveRequest();
		saveRequest.text = text;
		when(toDoService.upsert(ArgumentMatchers.any(ToDoSaveRequest.class))).thenReturn(
				ToDoEntityToResponseMapper.map(new ToDoEntity(text))
		);

		this.mockMvc.perform(MockMvcRequestBuilders
				.post("/todos")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(saveRequest))
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").exists())
				.andExpect(jsonPath("$.id").doesNotExist())
				.andExpect(jsonPath("$.text").value(saveRequest.text))
				.andExpect(jsonPath("$.completedAt").doesNotExist());
	}

	@Test
	void whenSaveWithId_thenReturnValidResponse() throws Exception {
		var saveRequest = new ToDoSaveRequest();
		saveRequest.id = 0L;
		saveRequest.text = "test todo";
		when(toDoService.upsert(ArgumentMatchers.any(ToDoSaveRequest.class))).thenReturn(
				ToDoEntityToResponseMapper.map(new ToDoEntity(saveRequest.id, saveRequest.text))
		);

		this.mockMvc.perform(MockMvcRequestBuilders
				.post("/todos")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(saveRequest))
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").exists())
				.andExpect(jsonPath("$.id").value(saveRequest.id))
				.andExpect(jsonPath("$.text").value(saveRequest.text))
				.andExpect(jsonPath("$.completedAt").doesNotExist());
	}

	@Test
	void whenComplete_thenReturnValidResponseWithCompletedAt() throws Exception {
		var todo = new ToDoEntity(0L, "test");
		todo.completeNow();
		var result = ToDoEntityToResponseMapper.map(todo);
		when(toDoService.completeToDo(anyLong())).thenReturn(result);

		this.mockMvc.perform(MockMvcRequestBuilders
				.put("/todos/{id}/complete", 0)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").exists())
				.andExpect(jsonPath("$.id").value(todo.getId()))
				.andExpect(jsonPath("$.text").value(todo.getText()))
				.andExpect(jsonPath("$.completedAt").exists());
	}

	@Test
	void whenGetOne_thenReturnValidResponse() throws Exception {
		var todo = new ToDoEntity(0L, "test text");
		todo.completeNow();
		when(toDoService.getOne(anyLong())).thenReturn(ToDoEntityToResponseMapper.map(todo));

		this.mockMvc
				.perform(get("/todos/{id}", 0))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").exists())
				.andExpect(jsonPath("$.id").isNumber())
				.andExpect(jsonPath("$.id").value(todo.getId()))
				.andExpect(jsonPath("$.text").value(todo.getText()))
				.andExpect(jsonPath("$.completedAt").exists());
	}
}
