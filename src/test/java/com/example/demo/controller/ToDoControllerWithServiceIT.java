package com.example.demo.controller;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static com.example.demo.helpers.JsonHelper.asJsonString;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
@Import(ToDoService.class)
class ToDoControllerWithServiceIT {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ToDoRepository toDoRepository;

	@Test
	void whenSaveNoId_thenReturnValidResponse() throws Exception {
		Long newId = 0L;
		String text = "test todo";
		var saveRequest = new ToDoSaveRequest();
		saveRequest.text = text;
		when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
			var toDoEntity = i.getArgument(0, ToDoEntity.class);
			if (toDoEntity.getId() == null) {
				return new ToDoEntity(newId, toDoEntity.getText());
			} else {
				return new ToDoEntity();
			}
		});

		this.mockMvc.perform(
				MockMvcRequestBuilders
				.post("/todos")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(saveRequest))
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").exists())
				.andExpect(jsonPath("$.id").value(newId))
				.andExpect(jsonPath("$.text").value(saveRequest.text))
				.andExpect(jsonPath("$.completedAt").doesNotExist());
	}

	@Test
	void whenSaveWithId_thenReturnValidResponse() throws Exception {
		var saveRequest = new ToDoSaveRequest();
		saveRequest.id = 0L;
		saveRequest.text = "test todo";
		when(toDoRepository.findById(anyLong())).thenAnswer(i -> {
			var id = i.getArgument(0, Long.class);
			if (id.equals(saveRequest.id)) {
				return Optional.of(new ToDoEntity(id, saveRequest.text));
			} else {
				return Optional.empty();
			}
		});
		when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
			var toDoEntity = i.getArgument(0, ToDoEntity.class);
			if (toDoEntity.getId() != null) {
				return new ToDoEntity(toDoEntity.getId(), toDoEntity.getText());
			} else {
				return new ToDoEntity();
			}
		});

		this.mockMvc.perform(MockMvcRequestBuilders
				.post("/todos")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(saveRequest))
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").exists())
				.andExpect(jsonPath("$.id").value(saveRequest.id))
				.andExpect(jsonPath("$.text").value(saveRequest.text))
				.andExpect(jsonPath("$.completedAt").doesNotExist());
	}

	@Test
	void whenSaveWithId_thenThrowNotFoundException() throws Exception {
		var saveRequest = new ToDoSaveRequest();
		saveRequest.id = 0L;
		saveRequest.text = "test todo";
		when(toDoRepository.findById(anyLong())).thenReturn(Optional.empty());

		this.mockMvc.perform(MockMvcRequestBuilders
				.post("/todos")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(saveRequest))
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Test
	void whenComplete_thenReturnValidResponseWithCompletedAt() throws Exception {
		var toDo = new ToDoEntity(0L, "test");
		toDo.completeNow();
		when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(toDo));
		when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
			var toDoEntity = i.getArgument(0, ToDoEntity.class);
			if (toDo.getId().equals(toDoEntity.getId())) {
				return toDo;
			} else {
				return new ToDoEntity();
			}
		});

		this.mockMvc.perform(MockMvcRequestBuilders
				.put("/todos/{id}/complete", 0)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").exists())
				.andExpect(jsonPath("$.id").value(toDo.getId()))
				.andExpect(jsonPath("$.text").value(toDo.getText()))
				.andExpect(jsonPath("$.completedAt").value(toDo.getCompletedAt().toString()));
	}

	@Test
	void whenComplete_thenThrowNotFoundException() throws Exception {
		when(toDoRepository.findById(anyLong())).thenReturn(Optional.empty());

		this.mockMvc.perform(MockMvcRequestBuilders
				.put("/todos/{id}/complete", 0))
				.andExpect(status().isNotFound());
	}

	@Test
	void whenGetOne_thenReturnValidResponse() throws Exception {
		var todo = new ToDoEntity(0L, "test text");
		todo.completeNow();
		when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(todo));

		this.mockMvc
				.perform(get("/todos/{id}", 0))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").exists())
				.andExpect(jsonPath("$.id").isNumber())
				.andExpect(jsonPath("$.id").value(todo.getId()))
				.andExpect(jsonPath("$.text").value(todo.getText()))
				.andExpect(jsonPath("$.completedAt").exists());

	}

	@Test
	void whenGetOne_thenThrowNotFoundException() throws Exception {
		when(toDoRepository.findById(anyLong())).thenReturn(Optional.empty());

		this.mockMvc
				.perform(get("/todos/{id}", 0))
				.andExpect(status().isNotFound());
	}

	@Test
	void whenDeleteOne_thenReturnValidStatus() throws Exception {
		this.mockMvc
				.perform(MockMvcRequestBuilders
				.delete("/todos/0"))
				.andExpect(status().isOk());
	}

}
