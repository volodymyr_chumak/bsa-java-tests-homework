package com.example.demo.dto.mapper;

import com.example.demo.model.ToDoEntity;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class ToDoEntityToResponseMapperTest {
    @Test
    void whenMapUncompleted_thenReturnResponse() {
        var toDoEntity = new ToDoEntity(0L, "test entity");

        //call
        var toDoResponse = ToDoEntityToResponseMapper.map(toDoEntity);

        //validate
        assertEquals(toDoResponse.id, toDoEntity.getId());
        assertEquals(toDoResponse.text, toDoEntity.getText());
        assertNull(toDoResponse.completedAt);
    }

    @Test
    void whenMapCompleted_thenReturnResponse() {
        var toDoEntity = new ToDoEntity(0L, "test entity");
        toDoEntity.completeNow();

        //call
        var toDoResponse = ToDoEntityToResponseMapper.map(toDoEntity);

        //validate
        assertEquals(toDoResponse.id, toDoEntity.getId());
        assertEquals(toDoResponse.text, toDoEntity.getText());
        assertEquals(toDoResponse.completedAt, toDoEntity.getCompletedAt());
    }

}
